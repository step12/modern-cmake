# Find the merge base compared to master.
base=$(git merge-base master HEAD)
# Create an empty array that will contain all the filepaths of files modified.
modified_filepaths=()
#build_dir="RSIR_OUTPUT_MONITOR"
build_dir="examples/build-simple-project-Desktop-Default"
# To properly handle file names with spaces, we have to do some bash magic.
# We set the Internal Field Separator to nothing and read line by line.
while IFS='' read -r line
do
  # For each line of the git output, we call `realpath` to get the absolute path of the file.
  absolute_filepath=$(realpath "$line")
  echo "$absolute_filepath"
  
  # Append the absolute filepath.
  #if [[ $absolute_filepath == *.cpp ]]
  #then
    clang-tidy -p $build_dir "$absolute_filepath" -checks=* >> "$build_dir/clang-tidy-output"
    modified_filepaths+=("$absolute_filepath")
  #fi
# `git diff-tree` outputs all the files that differ between the different commits.
# By specifying `--diff-filter=d`, it doesn't report deleted files.
done < <(git diff-tree --no-commit-id --diff-filter=d --name-only -r "$base" HEAD)
# -m specifies that `parallel` should distribute the arguments evenly across the executing jobs.
# -p Tells clang-tidy where to find the `compile_commands.json`.
# `{}` specifies where `parallel` adds the command-line arguments.
# `:::` separates the command `parallel` should execute from the arguments it should pass to the commands.
# `| tee` specifies that we would like the output of clang-tidy to go to `stdout` and also to capture it in
# `$build_dir/clang-tidy-output` for later processing.

#parallel -m clang-tidy -p $build_dir {} ::: "${modified_filepaths[@]}" | tee "$build_dir/clang-tidy-output"
cat "$build_dir/clang-tidy-output" | ./clang-tidy-to-junit.py /srv/Jenkins/source-root-directory >"$build_dir/junit.xml"
#echo $modified_filepaths
