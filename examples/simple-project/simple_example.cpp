// This is an example file as part of Modern-CMake

#include "simple_lib.hpp"

#include <iostream>

int some_disgusting_code(int a, int b)
{char c = (char)a + (char)b;
return c;}

void BubbleSort () {
    using namespace std;
    int i, j,temp,pass=0;
    int a[10] = {10,2,0,14,43,25,18,1,5,45};
    cout <<"Input list ...\n";
    for(i = 0; i<10; i++) {
       cout <<a[i]<<"\t";
    }
 cout<<endl;
 for(i = 0; i<10; i++) {
    for(j = i+1; j<10; j++)
    {
       if(a[j] < a[i]) {
          temp = a[i];
          a[i] = a[j];
          a[j] = temp;
       }
    }
 pass++;
 }
 cout <<"Sorted Element List ...\n";
 for(i = 0; i<10; i++) {
    cout <<a[i]<<"\t";
 }
}

int main() {

    std::cout << "Simple example C++ compiled correctly and ran." << std::endl;
    std::cout << simple_lib_function() << std::endl;
    std::cout << " the end " << std::endl;

    return 0;
}
